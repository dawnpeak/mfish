/**
 * Created by xianglong on 2018/07/02.
 * 首页相关api
 */
import * as API from './'
export default {
  // 获取首页公告内容
  home: params => {
    return API.GET('/api/agent/home', params)
  }
}
