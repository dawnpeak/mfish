/**
 * Created by xianglong on 2018/07/03.
 * 任务管理相关api
 */
import * as API from './'

export default {
  // 任务列表
  findList: params => {
    return API.GET('/api/agent/task',params)
  },
  //任务明细
  taskDetail: params => {
    return API.GET('/api/agent/task/detail',params)
  },
  // 禁用任务
  taskStop: params => {
  	return API.GET('/api/agent/task/stop',params)
  },
  // 任务加速
  taskRank: params => {
  	return API.GET('/api/agent/task/rank',params)
  },  
  // 添加单个D音任务
  addTask: params => {
  	return API.POST('/api/agent/task/add',params)
  },
  addKsTask: params => {
  	return API.POST('/api/agent/kuaishoutask/add',params)
  },
  // 获取各任务的实时单价
  getPrice: params => {
  	return API.GET('/api/agent/task/priceinfo',params)
  },
}
