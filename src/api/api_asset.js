/**
 * Created by xianglong on 2018/07/02.
 * 资产管理相关api
 */
import * as API from './'

export default {
  getPackageList:params=>{
    return API.GET('/api/agent/pay/packageList', params)
  },
  // 获取充值二维码
  precreate: params => {
    return API.POST('/api/agent/pay/precreate', params)
  },
  // 查询支付状态
  query: params => {
    return API.POST('/api/agent/pay/query', params)
  },  
  // 资产变更记录
  balanceLog: params => {
  	return API.GET('/api/agent/balancelog',params)
  }
}