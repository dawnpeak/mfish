/**
 * Created by xianglong on 2018/07/02.
 * 推广管理相关api
 */
import * as API from './'

export default {
  // 获取客户的推广链接即分成比例
  getSpreadLink: params => {
  	return API.GET('/api/user/recommendinfo',params)
  },
  // 获取推广下线列表
  getDownlineList: params => {
  	return API.GET('/api/recommend',params)
  },
  // 获取用户佣金明细
  getRewardDetail: params =>{
  	return API.GET('/api/commision',params)
  },
  // ******
  // 提现管理  
  // ******
  // 
  // 设置支付宝账号
  setZhiFuBaoAccount: params => {
  	return API.POST('/api/user/account',params)
  },
  // 提现
  putForward: params => {
  	return API.POST('/api/commision/cash',params)
  },
  // 提现记录
  getCashLog: params => {
  	return API.GET('/api/commision/cashlog',params)
  }
}