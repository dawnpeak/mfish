/**
 * Created by xianglong on 2018/07/03.
 * 用户相关api
 */
import * as API from './'
export default {
  // 登录
  login: params => {
    return API.POST('/api/agent/user/login', params)
  },
  // 注册
  register: params => {
    return API.POST('/api/agent/user/register', params)
  },
  // 登出
  logout: params => {
    return API.POST('/api/agent/user/logout', params)
  },
  // 查询用户信息
  getUserInfo: params => {
    return API.GET('/api/agent/user', params)
  },
  // 修改密码
  changePwd: params => {
    return API.PUT('/api/agent/user/changepwd', params)
  }
}
